# versioner

A utility library that can scan for references to a given class, or trait and change its version

**Please keep a backup of your project or ensure its committed under version control before using this package**

This package may not keep the exact project formatting so be prepared to use your preferred formatter tool after
