<?php


namespace ComposePress\Versioner\Visitors;


use ComposePress\Versioner\Abstracts\ChangeNameVisitor;
use PhpParser\Node;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\Node\Stmt\TraitUse;

class ChangeTraitNameVisitor extends ChangeNameVisitor {


	/**
	 * @param \PhpParser\Node $node
	 *
	 * @return \PhpParser\Node\Identifier|null
	 */
	public function leaveNode( Node $node ) {
		if ( ( $node instanceof TraitUse ) ) {
			/** @var TraitUse $node */
			foreach ( $node->traits as $trait ) {
				$node->name                 = $this->process_name( $trait, false );
				$this->partial_namespaces[] = (string ) $node->name;
				$this->partial_namespaces   = array_unique( $this->partial_namespaces );
			}

		}
		if ( $node instanceof Trait_ ) {
			$this->process_type_statement( $node );
		}

		if ( $node instanceof Node\Name ) {
			return $this->process_name( $node );
		}

		return null;
	}
}
