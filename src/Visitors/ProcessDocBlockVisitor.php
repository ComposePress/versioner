<?php


namespace ComposePress\Versioner\Visitors;


use ComposePress\Versioner\Abstracts\ChangeNameVisitor;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlock\Serializer;
use phpDocumentor\Reflection\DocBlock\Tags\BaseTag;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use phpDocumentor\Reflection\DocBlockFactory;
use phpDocumentor\Reflection\Fqsen;
use phpDocumentor\Reflection\Type;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Compound;
use phpDocumentor\Reflection\Types\Context;
use phpDocumentor\Reflection\Types\Object_;
use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Name;
use PhpParser\Node\Name\FullyQualified;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\Node\Stmt\UseUse;

/**
 * Class ProcessDocBlockVisitor
 *
 * @package ComposePress\Versioner\Visitors
 */
class ProcessDocBlockVisitor extends ChangeNameVisitor {
	/**
	 * @var \phpDocumentor\Reflection\DocBlockFactory
	 */
	protected $doc_block_factory;

	/**
	 * ProcessDocBlockVisitor constructor.
	 *
	 * @param string $name
	 * @param string $version
	 */
	public function __construct( string $name, string $version ) {
		parent::__construct( $name, $version );
		$this->doc_block_factory = DocBlockFactory::createInstance();
	}

	/**
	 * @param \PhpParser\Node $node
	 *
	 * @return int|null|\PhpParser\Node|\PhpParser\Node[]|void
	 */
	public function leaveNode( Node $node ) {

		if ( ( $node instanceof Class_ ) || ( $node instanceof Trait_ ) || ( $node instanceof Property ) || ( $node instanceof Variable ) ) {
			/** @var Node $node */
			/** @noinspection NotOptimalIfConditionsInspection */
			if ( $node instanceof Class_ ) {
				$this->process_type_doc( $node, 'class' );
			}
			/** @noinspection NotOptimalIfConditionsInspection */
			if ( $node instanceof Trait_ ) {
				$this->process_type_doc( $node, 'trait' );
			}
		}

		foreach (
			[
				Var_::class,
				DocBlock\Tags\Property::class,
				Param::class,
				Return_::class,
				DocBlock\Tags\Throws::class,
			] as $tag_type
		) {
			$this->process_and_save( $node, $tag_type );
		}


		if ( ( $node instanceof UseUse ) ) {
			/** @var UseUse $node */
			$this->partial_namespaces[] = (string ) $node->name;
			$this->partial_namespaces   = array_unique( $this->partial_namespaces );
		}

	}

	/**
	 * @param \PhpParser\Node $node
	 * @param string          $type
	 */
	protected function process_type_doc( Node $node, string $type ) {
		if ( ! ( $docblock = $this->get_docblock_from_node( $node ) ) ) {
			return;
		}
		$summary = $docblock->getSummary();
		if ( ! empty( $summary ) && false !== stripos( $summary, $type ) ) {
			$summary = trim( $summary );
			$summary = str_ireplace( $type, '', $summary );
			$summary = trim( $summary );

			$namespace = $this->get_closest_parent( $node, Node\Stmt\Namespace_::class );
			if ( $namespace ) {
				$namespace = (string) $namespace->name;
			}

			if ( empty( $summary ) ) {
				return;
			}
			$name = Name::concat( $namespace, $summary );
			/** @var Name $name */
			$name = $this->process_name( $name, false );

			$summary = str_replace( $summary, $name->getLast(), $docblock->getSummary() );
			$this->update_doc_block_and_node( $node, $docblock, [ 'summary' => $summary ] );
		}
	}

	/**
	 * @param \PhpParser\Node $node
	 *
	 * @return bool|\phpDocumentor\Reflection\DocBlock
	 */
	private function get_docblock_from_node( Node $node ) {
		$docblock      = $node->getDocComment();
		$docblock_text = (string) $docblock;
		if ( empty( $docblock ) ) {
			return false;
		}

		$namespace = $this->get_closest_parent( $node, Node\Stmt\Namespace_::class );

		if ( $namespace ) {
			$namespace = new Context( (string) $namespace->name );
		}

		return $this->doc_block_factory->create( $docblock_text, $namespace );
	}

	/**
	 * @param \PhpParser\Node $node
	 * @param string          $type
	 *
	 * @return null|\PhpParser\Node
	 */
	protected function get_closest_parent( Node $node, string $type ) {
		do {
			$node = $node->getAttribute( 'parent' );
		} while ( null !== $node && ! ( $node instanceof $type ) );

		if ( null === $node || ! ( $node instanceof $type ) ) {
			return null;
		}

		return $node;
	}

	/**
	 * @param \PhpParser\Node                    $node
	 * @param \phpDocumentor\Reflection\DocBlock $doc_block
	 * @param array                              $options
	 */
	private function update_doc_block_and_node( Node $node, DocBlock $doc_block, array $options ) {
		$doc_block = $this->update_doc_block( $doc_block, $options );
		$this->update_node_doc_block( $node, $doc_block );
	}

	/**
	 * @param \phpDocumentor\Reflection\DocBlock $doc_block
	 * @param array                              $options
	 *
	 * @return \phpDocumentor\Reflection\DocBlock
	 */
	private function update_doc_block( DocBlock $doc_block, array $options ) {
		$arguments = [ 'summary', 'description', 'tags', 'context', 'location', 'template_start', 'template_end' ];
		$getters   = [
			'getSummary',
			'getDescription',
			'getTags',
			'getContext',
			'getLocation',
			'isTemplateStart',
			'isTemplateEnd',
		];

		foreach ( $arguments as $position => $argument ) {
			if ( isset( $options[ $argument ] ) ) {
				$arguments[ $position ] = $options[ $argument ];
				continue;
			}
			$arguments[ $position ] = $doc_block->{$getters[ $position ]}();
		}

		return new DocBlock( ...$arguments );
	}

	/**
	 * @param \PhpParser\Node                    $node
	 * @param \phpDocumentor\Reflection\DocBlock $doc_block
	 */
	private function update_node_doc_block( Node $node, DocBlock $doc_block ) {
		$serializer = new Serializer();
		$node->setDocComment( new Doc( $serializer->getDocComment( $doc_block ) ) );
	}

	private function process_and_save( Node $node, $type ) {
		if ( ! ( $doc_block = $this->get_docblock_from_node( $node ) ) ) {
			return;
		}

		$tags = $this->process_tags( $doc_block, $type );
		$this->update_doc_block_and_node( $node, $doc_block, [ 'tags' => $tags ] );
	}

	private function process_tags( DocBlock $doc_block, string $type ) {
		$tags     = $doc_block->getTags();
		$new_tags = [];
		/** @var BaseTag $tag */
		foreach ( $tags as $tag ) {
			if ( ! ( $tag instanceof $type ) ) {
				$new_tags[] = $tag;
				continue;
			}
			if ( ! $tag->getType() ) {
				$new_tags[] = $tag;
				continue;
			}

			$new_type = $this->process_types( $tag->getType() );
			$reflect  = new \ReflectionClass( $type );
			$props    = $reflect->getProperties();
			$params   = $reflect->getConstructor()->getParameters();

			$arguments = [];
			foreach ( $params as $param ) {
				foreach ( $props as $prop ) {
					if ( $param->getName() === $prop->getName() ) {
						if ( 'type' === $prop->getName() ) {
							$arguments[] = $new_type;
							break;
						}
						$method = 'get' . ucfirst( $prop->getName() );
						if ( method_exists( $tag, $method ) ) {
							$arguments[] = $tag->{$method}();
							continue;
						}
						$method = 'is' . ucfirst( $prop->getName() );
						if ( method_exists( $tag, $method ) ) {
							$arguments[] = $tag->{$method}();
							continue;
						}
						$method = lcfirst( $prop->getName() );
						if ( method_exists( $tag, $method ) ) {
							$arguments[] = $tag->{$method}();
							continue;
						}
					}
				}
			}
			$new_tags[] = new $type( ...$arguments );
		}

		return $new_tags;
	}

	/**
	 * @param \phpDocumentor\Reflection\Type $type
	 *
	 * @return \phpDocumentor\Reflection\Type|\phpDocumentor\Reflection\Types\Compound|\phpDocumentor\Reflection\Types\Object_
	 */
	private function process_types( Type $type ) {
		if ( $type instanceof Compound ) {
			return $this->process_compound_type( $type );
		}

		return $this->process_type( $type );
	}

	/**
	 * @param \phpDocumentor\Reflection\Types\Compound $type
	 *
	 * @return \phpDocumentor\Reflection\Types\Compound
	 */
	private function process_compound_type( Compound $type ) {
		$new_types = [];
		foreach ( $type as $sub_type ) {
			$new_types[] = $this->process_type( $sub_type );
		}

		return new Compound( $new_types );
	}

	/**
	 * @param \phpDocumentor\Reflection\Type $type
	 *
	 * @return \phpDocumentor\Reflection\Type|\phpDocumentor\Reflection\Types\Object_
	 */
	private function process_type( Type $type ) {
		if ( ( $type instanceof Array_ ) ) {
			return new Array_( $this->process_types( $type->getValueType() ), $this->process_types( $type->getKeyType() ) );
		}
		if ( ! ( $type instanceof Object_ ) ) {
			return $type;
		}
		/** @var Object_ $sub_type */
		$fqsen = $type->getFqsen();
		if ( $fqsen === null ) {
			return $type;
		}
		$name = Name::concat( ltrim( (string) $fqsen, '\\' ), null );
		if ( $name->isQualified() ) {
			$name = FullyQualified::concat( $name, null );
		}
		$name     = $this->process_name( $name, false );
		$new_name = (string) $name;
		$new_name = '\\' . ltrim( $new_name, '\\' );

		return new Object_( new Fqsen( $new_name ) );
	}
}
