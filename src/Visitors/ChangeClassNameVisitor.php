<?php


namespace ComposePress\Versioner\Visitors;


use ComposePress\Versioner\Abstracts\ChangeNameVisitor;
use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\AssignOp;
use PhpParser\Node\Expr\AssignRef;
use PhpParser\Node\Expr\BinaryOp;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\Ternary;
use PhpParser\Node\Expr\Yield_;
use PhpParser\Node\Expr\YieldFrom;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\GroupUse;
use PhpParser\Node\Stmt\UseUse;
use function ComposePress\Versioner\strip_version;

/**
 * Class ChangeClassNameVisitor
 *
 * @package ComposePress\Versioner\Visitors
 */
class ChangeClassNameVisitor extends ChangeNameVisitor {

	/**
	 * @param \PhpParser\Node $node
	 *
	 * @return \PhpParser\Node\Identifier|null
	 */
	public function leaveNode( Node $node ) {
		if ( $node instanceof Class_ ) {
			/** @var Class_ $node */
			$this->process_type_statement( $node );
			if ( null !== $node->extends ) {
				$node->extends = $this->process_name( $node->extends );
			}
		}
		if ( $node instanceof New_ ) {
			/** @var New_ $node */
			$this->process_class_expression( $node );
		}
		if ( $node instanceof String_ ) {
			/** @var String_ $node */
			$this->process_class_expression( $node );
		}
		if ( ( $node instanceof GroupUse ) ) {
			/** @var GroupUse $node */
			foreach ( $node->uses as $use ) {
				$use->name = $this->process_name( $use->name, false, (string) $node->prefix );
			}
		}
		if ( ( $node instanceof UseUse ) ) {
			/** @var UseUse $node */
			$node->name                 = $this->process_name( $node->name, false );
			$this->partial_namespaces[] = (string ) $node->name;
			$this->partial_namespaces   = array_unique( $this->partial_namespaces );
		}

		if ( $node instanceof Expr ) {
			/** @var Expression $node */
			$this->process_class_expression( $node );
		}

		return null;
	}

	/**
	 * @param Node|Node[] $expr
	 */
	private function process_class_expression( $expr ) {
		$classes = [
			Assign::class,
			AssignRef::class,
			AssignOp::class,
			Yield_::class,
			YieldFrom::class,
			BinaryOp::class,
			Ternary::class,
			Array_::class,
			ArrayDimFetch::class,
			Expr\Instanceof_::class,
		];

		if ( is_array( $expr ) ) {
			foreach ( $expr as $item ) {
				$this->process_class_expression( $item );
			}

			return;
		}

		if ( in_array( get_class( $expr ), $classes ) ) {
			foreach ( $expr->getSubNodeNames() as $node_name ) {
				if ( null !== $expr->$node_name ) {
					if ( $expr->$node_name instanceof Node\Name ) {
						$expr->$node_name = $this->process_name( $expr->$node_name );
						continue;
					}
					$this->process_class_expression( $expr->$node_name );
				}
			}
		}


		if ( ( $expr instanceof ClassConstFetch ) || ( $expr instanceof New_ ) ) {
			if ( null !== $expr->class ) {
				$expr->class = $this->process_name( $expr->class );
			}
		}
		if ( ( $expr instanceof String_ ) ) {
			if ( null !== $expr->value ) {
				$value  = $expr->value;
				$value  = strip_version( $value );
				$value  = '\\' . ltrim( $value, '\\' );
				$value2 = '\\' . ltrim( $this->name, '\\' );
				if ( $value === $value2 ) {
					$value       .= "_{$this->version}";
					$expr->value = $value;
				}
			}
		}
	}
}
