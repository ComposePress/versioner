<?php

namespace ComposePress\Versioner;

interface FileNameVersionerProgress {
	public function __invoke( string $name, array $data );
}
