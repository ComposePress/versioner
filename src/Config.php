<?php


namespace ComposePress\Versioner;


use ComposePress\Versioner\Exception\Config as ConfigException;

/**
 * Class Config
 *
 * @package ComposePress\Versioner
 */
class Config {

	/**
	 *
	 */
	const FILENAME = 'versioner.json';

	/**
	 * @var array
	 */
	protected $classes = [];
	/**
	 * @var array
	 */
	protected $traits = [];
	/**
	 * @var array
	 */
	protected $files = [];

	/**
	 * @var string
	 */
	protected $location;

	/**
	 * @var bool[]
	 */
	protected $process;

	/**
	 * @var string[]
	 */
	protected $autoload = [];

	/**
	 * @var array
	 */
	protected $exclude_rename = [];

	/**
	 * Config constructor.
	 *
	 * @param array       $config
	 * @param string|null $location
	 */
	public function __construct( array $config, string $location = null ) {
		if ( isset( $config['classes'] ) ) {
			$this->classes = array_filter( (array) $config['classes'] );
		}
		if ( isset( $config['traits'] ) ) {
			$this->traits = array_filter( (array) $config['traits'] );
		}
		if ( isset( $config['files'] ) ) {
			$this->files = array_filter( (array) $config['files'] );
		}
		if ( isset( $config['process'] ) ) {
			$this->process = array_filter( (array) $config['process'] );
		}
		if ( isset( $config['autoload'] ) ) {
			$this->autoload = array_filter( (array) $config['autoload'] );
		}
		if ( isset( $config['exclude_rename'] ) ) {
			$this->exclude_rename = array_filter( (array) $config['exclude_rename'] );
		}
		$this->location = $location;
	}

	/**
	 *
	 * @throws \ComposePress\Versioner\Exception\Config
	 */
	public static function find() {
		$file = getcwd() . '/' . self::FILENAME;
		while ( ! ( is_file( $file ) && is_readable( $file ) ) && ( '' !== dirname( $file ) && '.' !== dirname( $file ) && ( $file = dirname( $file ) ) ) ) {
			$file = $file . '/' . self::FILENAME;
		}
		if ( ! ( is_file( $file ) && is_readable( $file ) ) ) {
			throw new ConfigException( sprintf( 'Config file %s could not be found.', self::FILENAME ) );
		}

		return self::get_from_file( $file );
	}

	/**
	 * @param $file
	 *
	 * @return \ComposePress\Versioner\Config
	 */
	public static function get_from_file( $file ) {
		return self::get_from_json( file_get_contents( $file ), dirname( $file ) );
	}

	/**
	 * @param $json
	 *
	 * @return \ComposePress\Versioner\Config
	 */
	public static function get_from_json( $json, string $location = null ) {
		return new self( json_decode( $json, true ), $location );
	}

	/**
	 * @return string
	 */
	public function get_location(): string {
		return $this->location;
	}

	/**
	 * @return array
	 */
	public function get_classes(): array {
		return $this->classes;
	}

	/**
	 * @return array
	 */
	public function get_traits(): array {
		return $this->traits;
	}

	/**
	 * @return array
	 */
	public function get_files(): array {
		return $this->files;
	}

	/**
	 * @param $type
	 *
	 * @return bool
	 */
	public function is_processing_enabled( $type ): bool {
		if ( ! isset( $this->process[ $type ] ) || ! $this->process[ $type ] ) {
			return false;
		}

		return true;
	}

	/**
	 * @return string[]
	 */
	public function get_autoload(): array {
		return $this->autoload;
	}

	/**
	 * @return array
	 */
	public function get_exclude_rename(): array {
		return $this->exclude_rename;
	}
}
