<?php


namespace ComposePress\Versioner;


use ComposePress\Versioner\Abstracts\CodeVersioner;
use ComposePress\Versioner\Visitors\ChangeClassNameVisitor;
use ComposePress\Versioner\Visitors\ChangeTraitNameVisitor;
use ComposePress\Versioner\Visitors\ParentConnectorVisitor;
use ComposePress\Versioner\Visitors\ProcessDocBlockVisitor;
use PhpParser\NodeVisitor\NameResolver;

class DefaultCodeVersioner extends CodeVersioner {
	public function add_class_update( $class, $version ) {
		$this->add_visitor( new ChangeClassNameVisitor( $class, $version ) );
		$this->add_visitor( new ProcessDocBlockVisitor( $class, $version ) );
	}

	public function add_trait_update( $trait, $version ) {
		$this->add_visitor( new ChangeTraitNameVisitor( $trait, $version ) );
		$this->add_visitor( new ProcessDocBlockVisitor( $trait, $version ) );
	}

	protected function add_default_visitors() {
		$classes = array_map( 'get_class', $this->visitors );

		if ( ! in_array( NameResolver::class, $classes ) ) {
			array_unshift( $this->visitors, new NameResolver( null, [
				'replaceNodes' => false,
			] ) );
		}
		if ( ! in_array( ParentConnectorVisitor::class, $classes ) ) {
			array_unshift( $this->visitors, new ParentConnectorVisitor() );
		}
	}

}
