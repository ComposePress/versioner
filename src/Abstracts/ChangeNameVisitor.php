<?php


namespace ComposePress\Versioner\Abstracts;


use PhpParser\Node;
use PhpParser\Node\Name;
use PhpParser\Node\Name\FullyQualified;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\NodeVisitorAbstract;
use function ComposePress\Versioner\strip_version;

class ChangeNameVisitor extends NodeVisitorAbstract {
	/**
	 * @var string
	 */
	protected $name;
	/**
	 * @var string
	 */
	protected $version;

	/**
	 * @var \phpDocumentor\Reflection\DocBlockFactory
	 */
	protected $doc_block_factory;

	protected $partial_namespaces = [];

	/**
	 * ChangeClassNameVisitor constructor.
	 *
	 * @param string $name
	 * @param string $version
	 */
	public function __construct( $name, $version ) {
		$this->name    = ltrim( $name, '\\' );
		$this->version = str_replace( [ '.', '-' ], '_', $version );

	}

	protected function process_type_statement( Node $node ) {
		$full_name = (string) $node->namespacedName;
		$full_name = strip_version( $full_name );
		if ( $full_name === $this->name ) {
			$name             = $node->namespacedName->getLast();
			$name             = strip_version( $name );
			$name             = "{$name}_{$this->version}";
			$node->name->name = $name;
			$node->name->setAttribute( 'processed', true );
		}
	}

	protected function process_name( Name $node, $use_resolvedName = true, $group_namespace = null ) {
		$full_name             = null;
		$partial_namespace_end = null;
		if ( $node->getAttribute( 'processed' ) ) {
			return $node;
		}
		if ( $use_resolvedName ) {
			$resolvedName = $node->getAttribute( 'resolvedName' );
			$full_name    = (string) $resolvedName;
		}
		if ( ! $use_resolvedName ) {
			$full_name = (string) $node;
		}

		if ( null !== $group_namespace ) {
			$full_name = $group_namespace . '\\' . $full_name;
		}

		$full_name = strip_version( $full_name );

		if ( $full_name !== $this->name ) {
			foreach ( $this->partial_namespaces as $partial_namespace ) {
				if ( false !== stripos( $this->name, $partial_namespace ) ) {
					$full_name             = $partial_namespace . '\\' . $full_name;
					$partial_namespace_end = $full_name;
					$partial_namespace_end = str_replace( [
						'\\' . $node->getLast(),
						$partial_namespace,
					], '', $partial_namespace_end );
					$partial_namespace_end = ltrim( $partial_namespace_end, '\\' );
					break;
				}
			}
		}
		if ( $full_name !== $this->name ) {
			return $node;
		}
		if ( $node instanceof FullyQualified || ( $node->getAttribute( "parent" ) instanceof TraitUse && $node->isQualified() ) || ( ! $use_resolvedName && $node->isQualified() ) ) {
			$name = $node->getLast();
			$name = strip_version( $name );
			$name = "{$name}_{$this->version}";

			$parts = [
				str_replace( '\\' . strip_version( $node->getLast() ), '', $full_name ),
				$name,
			];

			$namespace_count = (string) $node;
			$namespace_count = explode( '\\', $namespace_count );
			$namespace_count = count( $namespace_count );

			if ( $node instanceof FullyQualified && 1 < $namespace_count ) {
				$new_node = FullyQualified::concat( ...$parts );
				$new_node->setAttribute( 'processed', true );

				return $new_node;
			}

			if ( $node instanceof FullyQualified && 1 > $namespace_count ) {
				$new_node = FullyQualified::concat( null, $name );
				$new_node->setAttribute( 'processed', true );

				return $new_node;
			}

			if ( ( ! $use_resolvedName && $node->isQualified() ) || $node->getAttribute( "parent" ) instanceof TraitUse ) {
				$new_node = Name::concat( ...$parts );
				$new_node->setAttribute( 'processed', true );

				return $new_node;
			}
		}

		$name = $node->getLast();
		$name = strip_version( $name );
		$name = "{$name}_{$this->version}";

		if ( ! empty( $partial_namespace ) ) {
			$new_node = Name::concat( $partial_namespace_end, $name );
			$new_node->setAttribute( 'processed', true );

			return $new_node;
		}

		$new_node = Name::concat( null, $name );
		$new_node->setAttribute( 'processed', true );

		return $new_node;

	}
}
