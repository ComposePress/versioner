<?php


namespace ComposePress\Versioner\Abstracts;


use PhpParser\Error;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;

/**
 * Class CodeVersioner
 *
 * @package ComposePress\CodeVersioner
 */
abstract class CodeVersioner {
	/**
	 * @var \PhpParser\Parser
	 */
	protected $parser;
	/**
	 * @var \PhpParser\NodeTraverser
	 */
	protected $node_traverser;

	/**
	 * @var NodeVisitor[]
	 */
	protected $visitors = [];

	/**
	 * Versioner constructor.
	 */
	public function __construct() {
		$this->parser = ( new ParserFactory )->create( ParserFactory::PREFER_PHP5 );
	}

	/**
	 * @return string
	 * @throws \PhpParser\Error
	 */
	public function run( $code ) {
		try {
			$nodes = $this->parser->parse( $code );
		} catch ( Error $error ) {
			throw $error;
		}
		$this->add_default_visitors();
		$this->node_traverser = new NodeTraverser();

		foreach ( $this->visitors as $visitor ) {
			$this->node_traverser->addVisitor( $visitor );
		}
		$nodes          = $this->node_traverser->traverse( $nodes );
		$pretty_printer = new Standard;

		return $pretty_printer->prettyPrintFile( $nodes );
	}

	/**
	 *
	 */
	protected function add_default_visitors() {

	}

	/**
	 * @param \PhpParser\NodeVisitor $visitor
	 */
	public function add_visitor( NodeVisitor $visitor ) {
		$this->visitors[] = $visitor;
	}

	/**
	 * @return array
	 */
	public function get_visitors(): array {
		return $this->visitors;
	}

	/**
	 *
	 */
	public function reset_visitors() {
		$this->visitors = [];
	}


}
