<?php


namespace ComposePress\Versioner\Abstracts;

use ComposePress\Versioner\Config;

/**
 * Class FileVersioner
 *
 * @package ComposePress\Versioner
 */
abstract class FileVersioner {
	/**
	 * @var \ComposePress\Versioner\Abstracts\CodeVersioner
	 */
	protected $code_versioner;
	/**
	 * @var string
	 */
	protected $version;

	/**
	 * @var array
	 */
	protected $file_paths = [];
	/**
	 * @var string
	 */
	protected $config;

	/**
	 * FileVersioner constructor.
	 *
	 * @param \ComposePress\Versioner\Abstracts\CodeVersioner $code_versioner
	 * @param \ComposePress\Versioner\Config                  $config
	 * @param string                                          $version
	 */
	public function __construct( CodeVersioner $code_versioner, Config $config, string $version ) {
		$this->code_versioner = $code_versioner;
		$this->config         = $config;
		$this->version        = $version;

	}

	/**
	 *
	 */
	abstract public function run();

	/**
	 * @param string $file_path
	 */
	public function add_file_path( string $file_path ) {
		$this->file_paths[] = $file_path;
	}

	/**
	 * @return array
	 */
	public function get_file_paths(): array {
		return $this->file_paths;
	}
}
