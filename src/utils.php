<?php

namespace ComposePress\Versioner;

/**
 * @param $text
 *
 * @return null|string
 */
function strip_version( $text ) {
	if ( ! preg_match( '/_[0-9_]+?[0-9_a-zA-Z]+?$/U', $text, $version ) ) {
		return $text;
	}

	return str_replace( $version[0], '', $text );
}
