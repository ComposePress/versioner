<?php


namespace ComposePress\Versioner;


use ComposePress\Versioner\Abstracts\FileVersioner;
use Composer\Factory;
use Composer\IO\NullIO;
use PhpParser\Error;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Interface_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\NodeFinder;
use PhpParser\ParserFactory;
use Webmozart\Glob\Glob;
use Webmozart\Glob\Iterator\GlobIterator;

/**
 * Class FileNameVersioner
 *
 * @package ComposePress\Versioner
 * @property DefaultCodeVersioner $code_versioner
 */
class FileNameVersioner extends FileVersioner {

	const PROGRESS_START = 'progress_start';

	const PROGRESS_UPDATE = 'progress_update';
	/**
	 * @var array
	 */
	private $file_process_queue = [];

	/**
	 * @var array
	 */
	private $file_rename_queue = [];

	/**
	 * @var
	 */
	private $psr4_map;

	/**
	 * @var \PhpParser\Parser
	 */
	private $parser;
	/**
	 * @var \PhpParser\NodeFinder
	 */
	private $node_finder;
	/**
	 * @var FileNameVersionerProgress
	 */
	private $progress_callback;

	private $node_list = [];

	/**
	 * FileNameVersioner constructor.
	 *
	 * @param \ComposePress\Versioner\Abstracts\CodeVersioner $code_versioner
	 * @param \ComposePress\Versioner\Config                  $config
	 * @param string                                          $version
	 * @param FileNameVersionerProgress|null                  $progress_callback
	 */
	public function __construct( \ComposePress\Versioner\Abstracts\CodeVersioner $code_versioner, \ComposePress\Versioner\Config $config, string $version, FileNameVersionerProgress $progress_callback = null ) {
		parent::__construct( $code_versioner, $config, $version );
		$this->parser            = ( new ParserFactory )->create( ParserFactory::PREFER_PHP5 );
		$this->node_finder       = new NodeFinder();
		$this->progress_callback = $progress_callback;
	}

	/**
	 *
	 */
	public function run() {
		$this->add_file_paths_from_config();

		$iterators = [];

		foreach ( $this->file_paths as $file_path ) {
			$iterators[] = new GlobIterator( $this->config->get_location() . DIRECTORY_SEPARATOR . ltrim( $file_path, DIRECTORY_SEPARATOR ) );
		}

		foreach ( $this->combine_iterators( $iterators ) as $file ) {

			if ( ! ( $object = $this->get_object( $file ) ) ) {
				continue;
			}

			list( $root_path ) = $this->get_root_path( $object );

			$info = new \SplFileInfo( $file );

			if ( null === $root_path ) {
				continue;
			}

			$this->process_object( $info, $object );
		}

		$this->run_callback( self::PROGRESS_START, [ 'total' => count( $this->file_process_queue ) ] );

		$this->process_queues();
	}

	/**
	 *
	 */
	private function add_file_paths_from_config() {
		foreach ( $this->config->get_files() as $file ) {
			$this->file_paths[] = $file;
		}
		$this->file_paths = array_unique( $this->file_paths );
	}

	/**
	 * @param $iterators
	 *
	 * @return \Generator
	 */
	private function combine_iterators( $iterators ) {
		foreach ( $iterators as $iterator ) {
			foreach ( $iterator as $row ) {
				yield( $row );
			}
		}
	}

	/**
	 * @param string $file
	 *
	 * @return bool|array
	 */
	private function get_object( string $file ) {
		$info = new \SplFileInfo( $file );

		if ( 'php' !== $info->getExtension() ) {
			return false;
		}
		if ( ! $info->getRealPath() ) {
			return false;
		}
		$nodes          = $this->node_list[ $info->getRealPath() ] = $this->parser->parse( file_get_contents( $info->getRealPath() ) );
		$namespace_node = $this->node_finder->findFirstInstanceOf( $nodes, Namespace_::class );
		$class_node     = $this->node_finder->findFirstInstanceOf( $nodes, Class_::class );
		$trait_node     = $this->node_finder->findFirstInstanceOf( $nodes, Trait_::class );
		$interface_node = $this->node_finder->findFirstInstanceOf( $nodes, Interface_::class );
		$namespace      = '';
		$object         = '';
		if ( null === $namespace_node ) {
			$namespace = null;
		}

		if ( null !== $namespace ) {
			$namespace = $namespace_node->name;
		}
		if ( null === $namespace_node ) {
			$namespace = '';
		}

		$namespace = (string) $namespace;
		$namespace = rtrim( $namespace, '\\' ) . '\\';
		$namespace = '\\' . ltrim( $namespace, '\\' );
		if ( null !== $class_node ) {
			$object = $class_node->name->name;
		}
		if ( null !== $trait_node ) {
			$object = $trait_node->name->name;
		}
		if ( null !== $interface_node ) {
			$object = $interface_node->name->name;
		}

		$object = strip_version( $object );

		return [ $namespace, $object ];
	}

	/**
	 * @param $psr4_map
	 * @param $object
	 *
	 * @return array
	 */
	private function get_root_path( $object ): array {
		list( $namespace ) = $object;

		$root_path = null;
		foreach ( $this->get_psr4_map() as $psr4_namespace => $path ) {
			$psr4_namespace = rtrim( $psr4_namespace, '\\' ) . '\\';
			if ( false !== strpos( $namespace, $psr4_namespace ) ) {
				$root_path = $path;
				break;
			}
		}

		return [ $root_path, $namespace ];
	}

	/**
	 * @return array
	 */
	private function get_psr4_map(): array {
		if ( null === $this->psr4_map ) {
			$list            = [];
			$composer        = Factory::create( new NullIO() );
			$composer_config = $composer->getPackage();
			$autoload        = $composer_config->getAutoload();
			$autoload_dev    = $composer_config->getDevAutoload();
			if ( isset( $autoload['psr-4'] ) ) {
				$list = array_merge( $list, (array) $autoload['psr-4'] );
			}
			if ( isset( $autoload_dev['psr-4'] ) ) {
				$list = array_merge( $list, (array) $autoload_dev['psr-4'] );
			}
			$list = array_merge( $list, $this->config->get_autoload() );
			$keys = array_map( 'strlen', array_keys( $list ) );
			array_multisort( $keys, SORT_DESC, $list );
			$this->psr4_map = $list;
		}

		return $this->psr4_map;
	}

	/**
	 * @param \SplFileInfo $file
	 * @param              $object
	 * @param              $namespace
	 * @param              $root_path
	 *
	 * @return void
	 */
	private function process_object( \SplFileInfo $file, array $object ) {
		$nodes = $this->node_list[ $file->getRealPath() ];
		/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		$object    = implode( '', $object );
		$do_rename = array_filter( $this->config->get_exclude_rename(), function ( $item ) use ( $file ) {
			return Glob::match( $file->getRealPath(), $this->config->get_location() . DIRECTORY_SEPARATOR . ltrim( $item, '/' . DIRECTORY_SEPARATOR ) );
		} );
		$do_rename = empty( $do_rename );
		if ( $this->config->is_processing_enabled( 'classes' ) && $this->node_finder->findFirstInstanceOf( $nodes, Class_::class ) ) {
			$this->code_versioner->add_class_update( $object, $this->version );
			$this->file_process_queue[ $file->getRealPath() ] = true;
			if ( $do_rename ) {
				$this->file_rename_queue [ $file->getRealPath() ] = true;
			}

		}
		if ( $this->config->is_processing_enabled( 'traits' ) && $this->node_finder->findFirstInstanceOf( $nodes, Trait_::class ) ) {
			$this->code_versioner->add_trait_update( $object, $this->version );
			$this->file_process_queue[ $file->getRealPath() ] = true;
			if ( $do_rename ) {
				$this->file_rename_queue [ $file->getRealPath() ] = true;
			}
		}
		if ( $this->config->is_processing_enabled( 'interfaces' ) && $this->node_finder->findFirstInstanceOf( $nodes, Interface_::class ) ) {
			$this->file_process_queue[ $file->getRealPath() ] = true;
			if ( $do_rename ) {
				$this->file_rename_queue [ $file->getRealPath() ] = true;
			}
		}
	}

	/**
	 *
	 */
	private function process_queues() {
		$version = str_replace( [ '.', '-' ], '_', $this->version );

		foreach ( array_keys( $this->file_process_queue ) as $index => $file ) {
			$info = new \SplFileInfo( $file );
			try {
				$output = $this->code_versioner->run( file_get_contents( $info->getRealPath() ) );
			} catch ( Error $error ) {
				$this->run_callback( self::PROGRESS_UPDATE, [ 'progress' => $index + 1, 'file' => $file ] );
				continue;
			}
			file_put_contents( $info->getRealPath(), $output );

			if ( isset( $this->file_rename_queue[ $file ] ) ) {
				$filename     = $info->getBasename( '.php' );
				$filename     = strip_version( $filename );
				$filename     = "{$filename}_{$version}";
				$new_filename = $info->getPath() . DIRECTORY_SEPARATOR . $filename . '.' . $info->getExtension();
				rename( $info->getRealPath(), $new_filename );
			}
			$this->run_callback( self::PROGRESS_UPDATE, [ 'progress' => $index + 1, 'file' => $file ] );
		}
	}

	private function run_callback( $name, $data ) {
		if ( null === $this->progress_callback ) {
			return;
		}
		( $this->progress_callback )( $name, $data );
	}
}
