<?php

use ComposePress\Versioner\Config;
use ComposePress\Versioner\DefaultCodeVersioner;
use ComposePress\Versioner\FileNameVersioner;
use ComposePress\Versioner\FileNameVersionerProgress;

class FileNameVersionerTest extends \Codeception\Test\Unit {
	const LIBRARY_PATH = __DIR__ . '/../_support/test_library';
	const LIBRARY_SRC = self::LIBRARY_PATH . '/src';

	/**
	 * @throws \ComposePress\Versioner\Exception\Config
	 */
	public function test_run() {

		$class = '<?php

namespace Test_Library;

use Test_Library\MyCoolInterface;
class MyCoolLibrary extends \ComposePress\Core\Abstracts\Component_1_2_3 {

	use MyCoolTrait;
	
	/**
	 * @var \Test_Library\MyCoolLibrary
	 */
	protected $my_property;
}';

		$class_result = '<?php

namespace Test_Library;

use Test_Library\MyCoolInterface;
class MyCoolLibrary_1_0_0 extends \ComposePress\Core\Abstracts\Component_1_2_3
{
    use MyCoolTrait_1_0_0;
    /**
     * @var \Test_Library\MyCoolLibrary_1_0_0
     */
    protected $my_property;
}';


		$trait            = '<?php
namespace Test_Library;

use Test_Library\MyCoolInterface;
use Test_Library\MyCoolLibrary;

trait MyCoolTrait {

}';
		$trait_result     = '<?php

namespace Test_Library;

use Test_Library\MyCoolInterface;
use Test_Library\MyCoolLibrary_1_0_0;
trait MyCoolTrait_1_0_0
{
}';
		$interface        = '<?php

namespace Test_Library;

use Test_Library\MyCoolLibrary;
interface MyCoolInterface {
}';
		$interface_result = '<?php

namespace Test_Library;

use Test_Library\MyCoolLibrary_1_0_0;
interface MyCoolInterface
{
}';

		file_put_contents( self::LIBRARY_SRC . '/MyCoolLibrary.php', $class );
		file_put_contents( self::LIBRARY_SRC . '/MyCoolTrait.php', $trait );
		file_put_contents( self::LIBRARY_SRC . '/MyCoolInterface.php', $interface );
		$filename_versioner = new FileNameVersioner( new DefaultCodeVersioner(), Config::find(), '1.0.0', new class implements FileNameVersionerProgress {
			public function __invoke( string $name, array $data ) {

			}
		} );
		$filename_versioner->run();
		$this->assertFileExists( self::LIBRARY_SRC . '/MyCoolLibrary_1_0_0.php' );
		$this->assertFileExists( self::LIBRARY_SRC . '/MyCoolTrait_1_0_0.php' );
		$this->assertFileExists( self::LIBRARY_SRC . '/MyCoolInterface.php' );
		$this->assertEquals( $class_result, @file_get_contents( self::LIBRARY_SRC . '/MyCoolLibrary_1_0_0.php' ) );
		$this->assertEquals( $trait_result, @file_get_contents( self::LIBRARY_SRC . '/MyCoolTrait_1_0_0.php' ) );
		$this->assertEquals( $interface_result, @file_get_contents( self::LIBRARY_SRC . '/MyCoolInterface.php' ) );
	}

	protected function _after() {
		$this->delete( self::LIBRARY_SRC );
		mkdir( self::LIBRARY_SRC );
		touch( self::LIBRARY_SRC . '/.gitkeep' );
	}

	protected function _before() {
		chdir( self::LIBRARY_PATH );
		$this->delete( self::LIBRARY_SRC );
		mkdir( self::LIBRARY_SRC );
		touch( self::LIBRARY_SRC . '/.gitkeep' );
	}

	private function delete( $files ) {
		if ( $files instanceof \Traversable ) {
			$files = iterator_to_array( $files, false );
		} elseif ( ! \is_array( $files ) ) {
			$files = [ $files ];
		}
		foreach ( $files as $file ) {
			if ( is_dir( $file ) ) {
				$this->delete( new \FilesystemIterator( $file, \FilesystemIterator::CURRENT_AS_PATHNAME | \FilesystemIterator::SKIP_DOTS ) );
				rmdir( $file );
			} else {
				unlink( $file );
			}
		}
	}
}
