<?php

use ComposePress\Versioner\DefaultCodeVersioner;

class DefaultCodeVersionerTest extends \Codeception\Test\Unit {
	/**
	 * @var \:UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	public function test_add_class_update() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$visitor = $versioner->get_visitors();
		$visitor = $visitor[0];
		$this->assertInstanceOf( 'ComposePress\Versioner\Visitors\ChangeClassNameVisitor', $visitor );
	}

	public function test_run_basic_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( "<?php

class test_1_0_0
{
}", $versioner->run( '<?php
		class test {
		
		}' ) );
	}

	public function test_run_namespaced_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'a\b\c\test', '1.0.0' );
		$this->assertEquals( "<?php

namespace a\b\c;

class test_1_0_0
{
}", $versioner->run( '<?php
		namespace a\b\c;
		class test {
		
		}' ) );
	}

	public function test_run_namespaced_class_prefixed_slash() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( "<?php

namespace a\b\c;

class test_1_0_0
{
}", $versioner->run( '<?php
		namespace a\b\c;
		class test {
		
		}' ) );
	}

	public function test_run_namespaced_class_with_phpdoc() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

/**
 * Class test_1_0_0
 *
 * @package a\b\c
 */
class test_1_0_0
{
}', $versioner->run( '<?php
namespace a\b\c;
/**
 * Class test
 *
 * @package a\b\c
 */
class test {

}' ) );
	}

	public function test_run_namespaced_class_with_phpdoc_existing_versioned_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

/**
 * Class test_1_0_0
 *
 * @package a\b\c
 */
class test_1_0_0
{
}', $versioner->run( '<?php
namespace a\b\c;
/**
 * Class test
 *
 * @package a\b\c
 */
class test {

}' ) );
	}

	public function test_run_class_with_phpdoc() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Class test_1_0_0
 *
 */
class test_1_0_0
{
}', $versioner->run( '<?php
/**
 * Class test
 */
class test {

}' ) );
	}

	public function test_run_new_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

class test_1_0_0
{
    public function get()
    {
        return new test_1_0_0();
    }
}', $versioner->run( '<?php
class test {
	public function get()
	{
		return new test();
	}
}' ) );
	}

	public function test_run_return_class_constant() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

class test_1_0_0
{
    public function get()
    {
        return test_1_0_0::A;
    }
}', $versioner->run( '<?php
class test {
	public function get()
	{
		return test::A;
	}
}' ) );
	}

	public function test_run_return_class_name_in_string() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

class test_1_0_0
{
    public function get()
    {
        return \'\\\test_1_0_0\';
    }
}', $versioner->run( '<?php
class test {
	public function get()
	{
		return \'\\\test\';
	}
}' ) );
	}

	public function test_run_new_class_namespaced() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        return new \a\b\c\test_1_0_0();
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		return new \a\b\c\test();
	}
}' ) );
	}

	public function test_run_new_class_namespaced_no_match() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        return new \a\b\test();
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		return new \a\b\test();
	}
}' ) );
	}

	public function test_run_assign_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        $a = new \a\b\c\test_1_0_0();
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		$a = new \a\b\c\test();
	}
}' ) );
	}

	public function test_run_assign_ref_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        $a =& new \a\b\c\test_1_0_0();
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		$a =& new \a\b\c\test();
	}
}' ) );
	}

	public function test_run_assign_op_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    const A = 1;
    public function get()
    {
        1 / \a\b\c\test_1_0_0::A;
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	const A = 1;
	public function get()
	{
		 1 / \a\b\c\test::A;
	}
}' ) );
	}

	public function test_run_yield_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    const A = 1;
    public function get()
    {
        while (1) {
            (yield \a\b\c\test_1_0_0::A);
        }
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	const A = 1;
	public function get()
	{
		while(1) {
			yield \a\b\c\test::A;
		}
	}
}' ) );
	}

	public function test_run_turnary_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        1 === 2 ? 1 : new \a\b\c\test_1_0_0();
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		 1 === 2 ? 1 : new \a\b\c\test();
	}
}' ) );
	}

	public function test_run_array_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    public function get()
    {
        ["g" => new \a\b\c\test_1_0_0()];
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	public function get()
	{
		 ["g" => new \a\b\c\test()];
	}
}' ) );
	}

	public function test_run_array_fetch_class() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

namespace a\b\c;

class test_1_0_0
{
    const A = 0;
    public function get()
    {
        $a = [new \a\b\c\test_1_0_0()];
        $b = [1 => $a[\a\b\c\test_1_0_0::A]];
    }
}', $versioner->run( '<?php
namespace a\b\c;
class test {
	const A = 0;
    public function get()
	{
         $a = [new \a\b\c\test()];
         $b = [1 => $a[\a\b\c\test::A]];
    }
}' ) );
	}

	public function test_run_class_import() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( "<?php

use test_1_0_0;
class blah
{
}", $versioner->run( '<?php
		use test;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_namespaced() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c\test_1_0_0;
class blah
{
}', $versioner->run( '<?php
		use a\b\c\test;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_aliased() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\test', '1.0.0' );
		$this->assertEquals( '<?php

use test_1_0_0 as test2;
class blah
{
}', $versioner->run( '<?php
		use test as test2;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_namespaced_aliased() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c\test_1_0_0 as test2;
class blah
{
}', $versioner->run( '<?php
		use a\b\c\test as test2;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_versioned() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.1.0' );
		$this->assertEquals( '<?php

use test_1_1_0;
class blah
{
}', $versioner->run( '<?php
		use test_1_0_0;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_namespaced_versioned() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.1.0' );
		$this->assertEquals( '<?php

use a\b\c\test_1_1_0;
class blah
{
}', $versioner->run( '<?php
		use a\b\c\test_1_0_0;
		class blah {
		
		}' ) );
	}

	public function test_run_class_import_multiple_comma() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

use test_1_0_0 as test2, blah2;
class blah_1_0_0
{
}', $versioner->run( '<?php
		use test as test2, blah2;
		class blah_1_0_0 {
		
		}' ) );
	}

	public function test_run_class_import_multiple_comma_inverted() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

use blah2, test_1_0_0 as test2;
class blah_1_0_0
{
}', $versioner->run( '<?php
		use blah2, test_1_0_0 as test2;
		class blah_1_0_0 {
		
		}' ) );
	}

	public function test_run_class_import_namespaced_multiple_comma() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c\test_1_0_0 as test2, blah2;
class blah_1_0_0
{
}', $versioner->run( '<?php
		use a\b\c\test as test2, blah2;
		class blah_1_0_0 {
		
		}' ) );
	}


	public function test_run_class_import_namespaced_multiple_group() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c\{test_1_0_0, blah2};
class blah_1_0_0
{
}', $versioner->run( '<?php
		use a\b\c\{test, blah2};
		class blah_1_0_0 {
		
		}' ) );
	}

	public function test_run_class_import_namespaced_partial() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\d\e\f\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c;
class blah_1_0_0
{
    public function get()
    {
        return new d\e\f\test_1_0_0();
    }
}', $versioner->run( '<?php
		use a\b\c;
		class blah_1_0_0 {
			public function get(){
				return new d\e\f\test();
			}
		}' ) );
	}

	public function test_run_class_import_namespaced_partial_full_mix() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\c\d\e\f\test', '1.0.0' );
		$this->assertEquals( '<?php

use a\b\c;
class blah_1_0_0
{
    public function get()
    {
        return new \a\b\c\d\e\f\test_1_0_0();
    }
}', $versioner->run( '<?php
		use a\b\c;
		class blah_1_0_0 {
			public function get(){
				return new \a\b\c\d\e\f\test();
			}
		}' ) );
	}


	public function test_extends() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

class blah_1_0_0 extends test_1_0_0
{
}', $versioner->run( '<?php
class blah_1_0_0 extends test_1_0_0 {

}' ) );
	}

	public function test_trait() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_trait_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

trait test_1_0_0
{
}', $versioner->run( '<?php
trait test {

}' ) );
	}

	public function test_trait_use() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_trait_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

trait blah
{
    use test_1_0_0;
}', $versioner->run( '<?php
trait blah {
	use test;
}' ) );
	}

	public function test_trait_use_namespaced() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_trait_update( 'a\b\c\test', '1.0.0' );
		$this->assertEquals( '<?php

trait blah
{
    use a\b\c\test_1_0_0;
}', $versioner->run( '<?php
trait blah {
   use a\b\c\test;
}' ) );
	}

	public function test_trait_phpdoc() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_trait_update( 'blah', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Trait blah_1_0_0
 *
 */
trait blah_1_0_0
{
    use test;
}', $versioner->run( '<?php

/**
 * Trait blah
 */
trait blah {
	use test;
}' ) );
	}

	public function test_method_parameter() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Class blah
 *
 */
class blah
{
    /**
     * @param \a\b\test_1_0_0 $var
     */
    public function get($var)
    {
    }
}', $versioner->run( '<?php

/**
 * Class blah
 */
class blah {
	/**
	 * @param \a\b\test $var
	 */
	public function get( $var ) {
		
	}
}' ) );
	}


	public function test_empty_class_php_doc() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Class
 *
 */
class blah
{
    public function get($var)
    {
    }
}', $versioner->run( '<?php

/**
 * Class
 */
class blah {
	public function get( $var ) {
		
	}
}' ) );
	}

	public function test_empty_trait_php_doc() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Trait
 *
 */
class blah
{
    public function get($var)
    {
    }
}', $versioner->run( '<?php

/**
 * Trait
 */
class blah {
	public function get( $var ) {
		
	}
}' ) );
	}

	public function test__method_parameter_and_return() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Class blah
 *
 */
class blah
{
    /**
     * @param \a\b\test_1_0_0 $var
     * @return void
     */
    public function get($var)
    {
    }
}', $versioner->run( '<?php

/**
 * Class blah
 */
class blah {
	/**
	 * @param \a\b\test $var
	 * @return void
	 */
	public function get( $var ) {
		
	}
}' ) );
	}

	public function test_compound_method_parameter() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * Class blah
 *
 */
class blah
{
    /**
     * @param \a\b\test_1_0_0|string $var
     */
    public function get($var)
    {
    }
}', $versioner->run( '<?php

/**
 * Class blah
 */
class blah {
	/**
	 * @param \a\b\test|string $var
	 */
	public function get( $var ) {
		
	}
}' ) );
	}

	public function test_function_parameter() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->assertEquals( '<?php

/**
 * @param \a\b\test_1_0_0 $var
 */
function get($var)
{
}', $versioner->run( '<?php
/**
 * @param \a\b\test_1_0_0 $var
 */
function get( $var ) {
	
}' ) );
	}

	public function test_syntax_error() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( '\a\b\test', '1.0.0' );
		$this->expectException( \PhpParser\Error::class );
		$versioner->run( '<?php func' );
	}

	public function test_run_semver_patch() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0-5.abc.1' );
		$this->assertEquals( "<?php

class test_1_0_0_5_abc_1
{
}", $versioner->run( '<?php
		class test {
		
		}' ) );
	}

	public function test_run_semver_patch_existing_version() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0-5.abc.10' );
		$this->assertEquals( "<?php

class test_1_0_0_5_abc_10
{
}", $versioner->run( '<?php
		class test_1_0_0_5_abc_9 {
		
		}' ) );
	}

	public function test_run_instanceof() {
		$versioner = new DefaultCodeVersioner();
		$versioner->add_class_update( 'test', '1.0.0' );
		$this->assertEquals( '<?php

class test_1_0_0
{
    public function get()
    {
        $a = 1;
        return $a instanceof test_1_0_0;
    }
}', $versioner->run( '<?php
class test {
	public function get()
	{
		$a = 1;
		return $a instanceof test;
	}
}' ) );
	}
}
