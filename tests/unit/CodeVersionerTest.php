<?php

use ComposePress\Versioner\Visitors\ParentConnectorVisitor;

class CodeVersionerTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
	}

	protected function _after() {
	}

	public function test_add_default_visitors() {
		$versioner = new DummyCodeVersioner();
		$this->assertEquals( '<?php

', $versioner->run( '' ) );
	}

	public function test_reset_visitors() {
		$versioner = new DummyCodeVersioner();
		$versioner->add_visitor( new ParentConnectorVisitor() );
		$this->assertCount( 1, $versioner->get_visitors() );
		$versioner->reset_visitors();
		$this->assertCount( 0, $versioner->get_visitors() );
	}
}
